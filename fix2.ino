    /////////////////////////////////////////////////////////////////
   //                 Arduino ILI9341 RTC                v1.01    //
  //       Get the latest version of the code here:              //
 //                 Create by Stalin de Comrade                 //
/////////////////////////////////////////////////////////////////


//Thư viện
#include "Adafruit_GFX.h"     
#include "Adafruit_ILI9341.h" 
#include  <SPI.h>
#include <Wire.h>


//Cấu hình chân
#define TFT_DC 9              
#define TFT_CS 10             
#define TFT_RST 8             
#define TFT_MISO 12           
#define TFT_MOSI 11           
#define TFT_CLK 13          

//Class
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_CLK, TFT_RST, TFT_MISO);

//Biến và biến cần in
char temperatureChar[10];
float temperature = 0;
float previousTemperature = 3;

String dateString;


void setup(){

  //Serial
  Serial.begin(9600);
  //TFT
  tft.begin();                      
  tft.setRotation(0);            
  tft.fillScreen(ILI9341_BLACK);

  //IN
  printText("MAY DO CHI",ILI9341_ORANGE,30,30,3);
  printText("ENCODER 1", ILI9341_GREEN,50,130,3);
  printText("ENCODER 2", ILI9341_RED,10,260,2);
  printText("ENCODER 3", ILI9341_BLUE,130,260,2);

}

void loop(){
    Serial.println("abc");
   if(temperature != previousTemperature)
  {
    previousTemperature = temperature;
    Serial.println(temperature);
    String temperatureString = String(temperature,1);
    temperatureString.toCharArray(temperatureChar,10);
    temperature += 2;

    //ENC1
    tft.fillRect(50,175,50,20,ILI9341_BLACK);
    printText(temperatureChar, ILI9341_WHITE,50,180,2);
    printText("", ILI9341_WHITE,158,175,3);
    printText("cm", ILI9341_WHITE,150,180,2);
        temperature += 2;

    //ENC2
    tft.fillRect(10,280,50,20,ILI9341_BLACK);
    printText(temperatureChar, ILI9341_WHITE,10,280,2);
//    printText("", ILI9341_WHITE,158,175,3);
//    printText("cm", ILI9341_WHITE,150,180,2);
    temperature += 3;

    //ENC3
    tft.fillRect(130,280,50,20,ILI9341_BLACK);
    printText(temperatureChar, ILI9341_WHITE,130,280,2);
//    printText("", ILI9341_WHITE,158,175,3);
//    printText("cm", ILI9341_WHITE,150,180,2);
    temperature += 1;
    delay(100);
}
}


//Hàm in 
void printText(char *text, uint16_t color, int x, int y,int textSize)
{
  tft.setCursor(x, y);
  tft.setTextColor(color);
  tft.setTextSize(textSize);
  tft.setTextWrap(true);
  tft.print(text);
}