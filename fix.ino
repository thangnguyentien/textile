    /////////////////////////////////////////////////////////////////
   //                 Arduino ILI9341 RTC                v1.01    //
  //       Get the latest version of the code here:              //
 //                 Create by Stalin de Comrade                 //
/////////////////////////////////////////////////////////////////


//Thư viện
#include "Adafruit_GFX.h"     
#include "Adafruit_ILI9341.h" 
#include  <SPI.h>

//Cấu hình chân
#define TFT_DC 9              
#define TFT_CS 10             
#define TFT_RST 8             
#define TFT_MISO 12           
#define TFT_MOSI 11           
#define TFT_CLK 13          

//Class
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_CLK, TFT_RST, TFT_MISO);

//Biến và biến cần in
uint32_t time, ptime = 0 ;
char timechar[50];

String myString;

void setup(){

  //Serial
  Serial.begin(9600);
  //TFT
  tft.begin();                      
  tft.setRotation(0);            
  tft.fillScreen(ILI9341_BLACK);

  //IN
  printText("Chi 1", ILI9341_GREEN,20,130,3);
  printText("Chi 2", ILI9341_RED,37,260,2);
  printText("Chi 3", ILI9341_BLUE,173,260,2);

}

void loop(){
   time = millis();
   Serial.println(time); 
   if ((time - ptime) > 1000){
       tft.fillRect(50,175,150,40,ILI9341_BLACK);
       myString.toCharArray(timechar, l0) 
       printText(timechar, ILI9341_WHITE,50,180,4);
       ptime = time;
   }
}


//Hàm in 
void printText(char *text, uint16_t color, int x, int y,int textSize)
{
  tft.setCursor(x, y);
  tft.setTextColor(color);
  tft.setTextSize(textSize);
  tft.setTextWrap(true);
  tft.print(text);
}
