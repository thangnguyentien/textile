#include "AiEsp32RotaryEncoder.h"
#define ROTARY_ENCODER_A_PIN 32
#define ROTARY_ENCODER_B_PIN 21
#define ROTARY_ENCODER_C_PIN 19
#define ROTARY_ENCODER_D_PIN 18
#define ROTARY_ENCODER_E_PIN 4
#define ROTARY_ENCODER_F_PIN 2
#define ROTARY_ENCODER_BUTTON_PIN 25
#define ROTARY_ENCODER_STEPS 4
AiEsp32RotaryEncoder rotaryEncoder = AiEsp32RotaryEncoder(ROTARY_ENCODER_A_PIN, ROTARY_ENCODER_B_PIN, ROTARY_ENCODER_BUTTON_PIN, -1, ROTARY_ENCODER_STEPS);
AiEsp32RotaryEncoder rotaryEncoder2 = AiEsp32RotaryEncoder(ROTARY_ENCODER_C_PIN, ROTARY_ENCODER_D_PIN, ROTARY_ENCODER_BUTTON_PIN, -1, ROTARY_ENCODER_STEPS);
AiEsp32RotaryEncoder rotaryEncoder3 = AiEsp32RotaryEncoder(ROTARY_ENCODER_E_PIN, ROTARY_ENCODER_F_PIN, ROTARY_ENCODER_BUTTON_PIN, -1, ROTARY_ENCODER_STEPS);

void rotary_onButtonClick()
{
    Serial.println("button pressed");
}

void setup()
{
    Serial.begin(115200);
    rotaryEncoder.begin();
    rotaryEncoder2.begin();
    rotaryEncoder3.begin();
    //ENC 1
    rotaryEncoder.setup(
        [] { rotaryEncoder.readEncoder_ISR(); },
        [] { rotary_onButtonClick(); });
    rotaryEncoder.setBoundaries(0, 60000, false); //minValue, maxValue, circleValues true|false (when max go to min and vice versa)
    rotaryEncoder.setAcceleration(1);
    //ENC 2
    rotaryEncoder2.setup(
        [] { rotaryEncoder2.readEncoder_ISR(); },
        [] { rotary_onButtonClick(); });
    rotaryEncoder2.setBoundaries(0, 60000, false); //minValue, maxValue, circleValues true|false (when max go to min and vice versa)
    rotaryEncoder2.setAcceleration(1);
    //ENC 3
    rotaryEncoder3.setup(
        [] { rotaryEncoder3.readEncoder_ISR(); },
        [] { rotary_onButtonClick(); });
    rotaryEncoder3.setBoundaries(0, 60000, false); //minValue, maxValue, circleValues true|false (when max go to min and vice versa)
    rotaryEncoder3.setAcceleration(1);
}

void loop()
{
    if (rotaryEncoder.encoderChanged())
    {
        Serial.print("Encoder 1 ");
        Serial.println(rotaryEncoder.readEncoder());
    }
    if (rotaryEncoder2.encoderChanged())
    {
        Serial.print("Encoder 2");
        Serial.println((rotaryEncoder2.readEncoder()/600)*3.1415926*16);
    }
    if (rotaryEncoder3.encoderChanged())
    {
        Serial.print("Encoder 3 ");
        Serial.println(rotaryEncoder3.readEncoder());
        Serial.println((rotaryEncoder3.readEncoder()/600)*3.1415926*16);
    }
}
