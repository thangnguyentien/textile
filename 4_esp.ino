#include "AiEsp32RotaryEncoder.h"
#define ROTARY_ENCODER_A_PIN 32
#define ROTARY_ENCODER_B_PIN 21
#define ROTARY_ENCODER_BUTTON_PIN 25
#define ROTARY_ENCODER_STEPS 4

#define RXD2 16
#define TXD2 17

AiEsp32RotaryEncoder rotaryEncoder = AiEsp32RotaryEncoder(ROTARY_ENCODER_A_PIN, ROTARY_ENCODER_B_PIN, ROTARY_ENCODER_BUTTON_PIN, -1, ROTARY_ENCODER_STEPS);

void rotary_onButtonClick()
{
    Serial.println("button pressed");
}

uint16_t number1 = 0 ;
uint16_t number2 = 0 ;
uint16_t number3 = 0 ;
  
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
  
  rotaryEncoder.begin();
    rotaryEncoder.setup(
        [] { rotaryEncoder.readEncoder_ISR(); },
        [] { rotary_onButtonClick(); });
    rotaryEncoder.setBoundaries(0, 10000, false); //minValue, maxValue, circleValues true|false (when max go to min and vice versa)
    rotaryEncoder.setAcceleration(1);
}

void loop() {
  if (rotaryEncoder.encoderChanged())
    {
        number1 = rotaryEncoder.readEncoder() ;
        Serial.println(rotaryEncoder.readEncoder());
    }
    
  //Cách 1 dùng string
//  Serial.print(number); // Data truyền đi có 5 bytes
  //Cách 2 dùng 2 byte
  byte data[6];
  data[0] = 0x0FF & (number1 >>8); //0x30
  data[1] = 0x0FF & number1; //0x39
  data[2] = 0x0FF & (number2>> 8); //0x30
  data[3] = 0x0FF & number2; //0x39
  data[4] = 0x0FF & (number3 >> 8); //0x30
  data[5] = 0x0FF & number3; //0x39
  
//  Serial.println(data[0]);
//  Serial.println(data[0] << 8);
//  Serial.println(data[1]);
  Serial.println(data[1]+(data[0] << 8));
  Serial2.write(data, 6);
  delay(200);
}
