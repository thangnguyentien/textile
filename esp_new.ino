//Thư viện esp32-encoder
#include "AiEsp32RotaryEncoder.h"

//Cấu hình chân kết nối esp32-encoder
#define ROTARY_ENCODER_A_PIN 32
#define ROTARY_ENCODER_B_PIN 21
#define ROTARY_ENCODER_C_PIN 19
#define ROTARY_ENCODER_D_PIN 18
#define ROTARY_ENCODER_E_PIN 4
#define ROTARY_ENCODER_F_PIN 2
#define ROTARY_ENCODER_BUTTON_PIN 25
#define ROTARY_ENCODER_STEPS 4

//Cấu hình chân kết nối UART


//Class dùng cho esp32
AiEsp32RotaryEncoder rotaryEncoder = AiEsp32RotaryEncoder(ROTARY_ENCODER_A_PIN, ROTARY_ENCODER_B_PIN, ROTARY_ENCODER_BUTTON_PIN, -1, ROTARY_ENCODER_STEPS);
AiEsp32RotaryEncoder rotaryEncoder2 = AiEsp32RotaryEncoder(ROTARY_ENCODER_C_PIN, ROTARY_ENCODER_D_PIN, ROTARY_ENCODER_BUTTON_PIN, -1, ROTARY_ENCODER_STEPS);
AiEsp32RotaryEncoder rotaryEncoder3 = AiEsp32RotaryEncoder(ROTARY_ENCODER_E_PIN, ROTARY_ENCODER_F_PIN, ROTARY_ENCODER_BUTTON_PIN, -1, ROTARY_ENCODER_STEPS);

//Các biến cần thiết

//Biến 
uint16_t number1 = 0 ;          //Biến lưu giá trị ENC1
uint16_t number2 = 0 ;          //Biến lưu giá trị ENC2
uint16_t number3 = 0 ;          //Biến lưu giá trị ENC3

//Setup
void setup()
{
    //Serial, encoder begin 
    Serial.begin(115200);                           //Khởi động serial,baudrate 115200
    Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);    //Khởi động COM ảo, TX2 RX2

    rotaryEncoder.begin();                          //Khởi động Class encoder
    rotaryEncoder2.begin();
    rotaryEncoder3.begin();

    //ENC1
    rotaryEncoder.setup(
        [] { rotaryEncoder.readEncoder_ISR(); },
        [] { rotary_onButtonClick(); });
    rotaryEncoder.setBoundaries(0, 60000, false); //minValue, maxValue, circleValues true|false (when max go to min and vice versa)
    rotaryEncoder.setAcceleration(1);
    
    //ENC2
    rotaryEncoder2.setup(
        [] { rotaryEncoder2.readEncoder_ISR(); },
        [] { rotary_onButtonClick(); });
    rotaryEncoder2.setBoundaries(0, 60000, false); //minValue, maxValue, circleValues true|false (when max go to min and vice versa)
    rotaryEncoder2.setAcceleration(1);

    //ENC3
    rotaryEncoder3.setup(
        [] { rotaryEncoder3.readEncoder_ISR(); },
        [] { rotary_onButtonClick(); });
    rotaryEncoder3.setBoundaries(0, 60000, false); //minValue, maxValue, circleValues true|false (when max go to min and vice versa)
    rotaryEncoder3.setAcceleration(1);
}

//Loop
void loop()
{   
    //Đọc ENC1 lưu vào biến number1
    if (rotaryEncoder.encoderChanged())
    {
        Serial.print("Encoder 1 ");
        Serial.println((rotaryEncoder.readEncoder()/600)*3.1415926*16);
        number1 = rotaryEncoder.readEncoder() ;
    }

    //Đọc ENC2 lưu vào biến number2
    if (rotaryEncoder2.encoderChanged())
    {
        Serial.print("Encoder 2");
        Serial.println((rotaryEncoder2.readEncoder()/600)*3.1415926*16);
        number2 = rotaryEncoder2.readEncoder();
    }

    //Đọc ENC3 lưu vào biến number3
    if (rotaryEncoder3.encoderChanged())
    {
        Serial.print("Encoder 3 ");
        Serial.println(rotaryEncoder3.readEncoder());
        Serial.println((rotaryEncoder3.readEncoder()/600)*3.1415926*16);
        number3 = rotaryEncoder3.readEncoder();    
    }

    //Uart working 
    byte data[6];
    data[0] = 0x0FF & (number1 >>8); //0x30
    data[1] = 0x0FF & number1; //0x39
    data[2] = 0x0FF & (number2>> 8); //0x30
    data[3] = 0x0FF & number2; //0x39
    data[4] = 0x0FF & (number3 >> 8); //0x30
    data[5] = 0x0FF & number3; //0x39

    // Truyền 
    Serial.println(data[1]+(data[0] << 8));
    Serial2.write(data, 6);
}


//Subroutine