//Thư viện Uno-TFT
#include "Adafruit_GFX.h"     
#include "Adafruit_ILI9341.h" 
#include  <SPI.h>
#include <Wire.h>

//Thư viện UART
#include <SoftwareSerial.h>


// Cấu hình chân TFT
#define TFT_DC 9              
#define TFT_CS 10             
#define TFT_RST 8             
#define TFT_MISO 12           
#define TFT_MOSI 11           
#define TFT_CLK 13 

//Cấu hình chân UART
#define Rx 2
#define Tx 3


//Các class cần dùng 
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_CLK, TFT_RST, TFT_MISO);      //Class màn hình

SoftwareSerial mySerial(Rx, Tx);        //Class software serial

//Các biến của ENC1
char number1Char[10];
uint16_t number1 = 0;
uint16_t pre_number1 = 1;

//Các biến của ENC2
char number2Char[10];
uint16_t number2 = 0;
uint16_t pre_number2 = 1;

//Các biến của ENC3
char number3Char[10];
uint16_t number3 = 0;
uint16_t pre_number3 = 1;


String dateString;

void setup(){
    //Serial
    Serial.begin(9600);

    //TFT
    tft.begin();                      
    tft.setRotation(0);            
    tft.fillScreen(ILI9341_BLACK);

    //IN
    printText("MAY DO CHI",ILI9341_ORANGE,30,30,3);
    printText("ENCODER 1", ILI9341_GREEN,50,130,3);
    printText("ENCODER 2", ILI9341_RED,10,260,2);
    printText("ENCODER 3", ILI9341_BLUE,130,260,2);


}   
    void loop(){

    byte data[6];
  
    if(mySerial.available()==6){
      mySerial.readBytes(data,6);
      }
    uint16_t number1 = (data[1] + (data[0] << 8));
    uint16_t number2 = (data[3] + (data[2] << 8));
    uint16_t number3 = (data[5] + (data[4] << 8));

    if(number1 != pre_number1)
  {
    pre_number1 = number1;
    Serial.println(number1);
    String number1String = String(number1,1);
    number1String.toCharArray(number1Char,10);

    //ENC1
    tft.fillRect(50,175,50,20,ILI9341_BLACK);
    printText(number1Char, ILI9341_WHITE,50,180,2);
    // printText("", ILI9341_WHITE,158,175,3);
    // printText("cm", ILI9341_WHITE,150,180,2);


    //ENC3
    tft.fillRect(130,280,50,20,ILI9341_BLACK);
    printText(number3Char, ILI9341_WHITE,130,280,2);
//    printText("", ILI9341_WHITE,158,175,3);
//    printText("cm", ILI9341_WHITE,150,180,2);
}
    //Hàm in giá trị ENC2
    if(number2 != pre_number2)
  {
    pre_number2 = number2;
    Serial.println(number2);
    String number2String = String(number2,1);
    number2String.toCharArray(number2Char,10);

    //ENC2
    tft.fillRect(10,280,50,20,ILI9341_BLACK);
    printText(number2Char, ILI9341_WHITE,10,280,2);
//    printText("", ILI9341_WHITE,158,175,3);
//    printText("cm", ILI9341_WHITE,150,180,2);
    }

    //Hàm in giá trị ENC3
    if(number3 != pre_number3)
  {
    pre_number3 = number3;
    Serial.println(number3);
    String number3String = String(number3,1);
    number3String.toCharArray(number3Char,10);

    //ENC3
    tft.fillRect(130,280,50,20,ILI9341_BLACK);
    printText(number3Char, ILI9341_WHITE,130,280,2);
//    printText("", ILI9341_WHITE,158,175,3);
//    printText("cm", ILI9341_WHITE,150,180,2);
    }
 }

//Hàm in 
void printText(char *text, uint16_t color, int x, int y,int textSize)
{
  tft.setCursor(x, y);
  tft.setTextColor(color);
  tft.setTextSize(textSize);
  tft.setTextWrap(true);
  tft.print(text);
}
